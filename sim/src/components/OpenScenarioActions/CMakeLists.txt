set(COMPONENT_NAME OpenScenarioActions)

add_compile_definitions(OPENSCENARIO_ACTIONS_LIBRARY)

add_openpass_target(
  NAME ${COMPONENT_NAME} TYPE library LINKAGE shared COMPONENT core

  HEADERS
    openScenarioActions.h
    src/oscActionsCalculation.h
    src/openScenarioActionsImpl.h
    src/oscActionsCalculation.h
    src/actionTransformRepository.h
    src/transformerBase.h
    src/transformTrajectory.h
    src/transformLaneChange.h
    src/transformSpeedAction.h

  SOURCES
    openScenarioActions.cpp
    src/oscActionsCalculation.cpp
    src/openScenarioActionsImpl.cpp
    src/oscActionsCalculation.cpp
    src/transformLaneChange.cpp
    src/transformSpeedAction.cpp

  LIBRARIES
    Qt5::Core
)
